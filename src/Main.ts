import * as PIXI from "pixi.js";

import StateMachine from "./lib/StateMachine";
import ServiceLocator from "./ServiceLocator";
import GameState from "./state/GameState";
import PreloaderState from "./state/PreloaderState";


export default class Main
{
    // #region Attributes //

    // private:

    private _app: PIXI.Application = null;
    private _stateMachine: StateMachine = null; 

    // Binded functions.
    private _bfOnWindowResized: any = null;

    // #endregion //


    // #region Properties //

    public get app(): PIXI.Application { return this._app; }
    
    // #endregion //


    // #region Methods //

    public constructor() {}

    public init(): void
    { 
        // Setup body.
        document.body.style.margin = "0px";
        document.body.style.overscrollBehaviorY = "contain";
        document.body.style.backgroundColor = "black";

        // PIXI renderer.
        this._app = new PIXI.Application( { 
            width: 1920,
            height: 1080 } );
        let mainDiv: HTMLDivElement = document.createElement( "div" );
        mainDiv.style.width = "100%";
        mainDiv.style.height = "100%";
        mainDiv.style.position = "fixed";
        mainDiv.style.display = "flex";
        mainDiv.style.justifyContent = "center";
        mainDiv.style.alignItems = "center";
        mainDiv.append( this._app.view );
        document.body.appendChild( mainDiv );

        this._stateMachine = new StateMachine();     
        let preloaderState: PreloaderState = new PreloaderState();
        preloaderState.onComplete.addOnce( this.onPreloader_Complete, this );
        this._stateMachine.add( main_state_id.PRELOADER, preloaderState );
        this._stateMachine.add( main_state_id.GAME, new GameState() );
        this._stateMachine.init();

        PIXI.Ticker.shared.add( ( dt: number ) => { this._stateMachine.onUpdate( dt ); } );

        // Entry point.
        this._stateMachine.request( main_state_id.PRELOADER );

        // Listen to events.
        this._bfOnWindowResized = this.onWindow_Resized.bind( this );
        window.addEventListener( "resize", this._bfOnWindowResized );

        this.onWindow_Resized();
    }

        private onPreloader_Complete(): void 
        {
            this._stateMachine.request( main_state_id.GAME );
        }

        private onWindow_Resized(): void
        {
            const kRatio: number = this._app.renderer.width / this._app.renderer.height;
            let w: number = null;
            let h: number = null;
            if ( window.innerWidth / window.innerHeight >= kRatio ) 
            {
                w = window.innerHeight * kRatio;
                h = window.innerHeight;
            } 
            else 
            {
                w = window.innerWidth;
                h = window.innerWidth / kRatio;
            }
            this._app.view.style.width = w.toString() + "px";
            this._app.view.style.height = h.toString() + "px";
        }

    public end(): void
    {
        // Cleanup events.
        window.removeEventListener( "resize", this._bfOnWindowResized );
        this._bfOnWindowResized = null;

        this._stateMachine.end();
        this._stateMachine = null;

        this._app.stage.removeChildren();
        this._app.destroy( true );
        this._app = null;
    }

    // #endregion //
}

const enum main_state_id
{
    PRELOADER = 0,
    GAME
}

// When the index page has finished loading, create our app.
window.onload = () =>
{
    ServiceLocator.main = new Main();
    ServiceLocator.main.init();
}