import State from "../lib/State";

import * as PIXI from "pixi.js";
import * as signals from "signals";
import ServiceLocator from "../ServiceLocator";


export default class PreloaderState extends State
{
    // #region Attributes //

    // private:

    private _root: PIXI.Container = null;
    
    // Signals.
    private _onComplete: signals.Signal = new signals.Signal();

    // #endregion //


    // #region Properties //

    // Signals.
    public get onComplete(): signals.Signal { return this._onComplete; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor() 
    { 
        super(); 
    }

    public onEnter(): void
    {
        this._root = new PIXI.Container();
        ServiceLocator.main.app.stage.addChild( this._root );

        // Show a loading message.
        let loadingText: PIXI.Text = new PIXI.Text( "Loading...", { fontSize: 42, fill: "white" } );
        loadingText.anchor.set( 0.5 );
        loadingText.position.set( ServiceLocator.main.app.screen.width * 0.5, ServiceLocator.main.app.screen.height * 0.5 );
        this._root.addChild( loadingText );

        // Preload game assets.
        let loader: PIXI.Loader = PIXI.Loader.shared;
        loader.add( "game_assets", "resources/game_assets.json" );
        loader.add( "fire_emitter", "resources/fire_emitter.json" );
        loader.onComplete.once( () => { this._onComplete.dispatch(); } );
        loader.load();
    }

    public onLeave(): void
    {
        // Cleanup events.
        this._onComplete.removeAll();

        ServiceLocator.main.app.stage.removeChild( this._root );
        this._root.destroy( { children: true } );
        this._root = null;

        super.onLeave();
    }

    // #endregion //
}