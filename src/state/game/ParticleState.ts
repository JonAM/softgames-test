import TaskState from "./TaskState";

import ServiceLocator from "../../ServiceLocator";
import * as PIXI from "pixi.js";
import * as particles from "pixi-particles";


export default class ParticleState extends TaskState
{
    // #region Attributes //

    // private:

    private _fireEmitter: particles.Emitter = null;

    // #endregion //


    // #region Methods //

    // public:

    public constructor() 
    { 
        super(); 
    }

    public onEnter(): void
    {
        super.onEnter();

        let bonfire: PIXI.Sprite = PIXI.Sprite.from(  PIXI.Loader.shared.resources[ "game_assets" ].textures[ "bonfire.png" ] );
        bonfire.anchor.set( 0.48, 0.29 );
        bonfire.position.set( ServiceLocator.main.app.screen.width * 0.5, ServiceLocator.main.app.screen.height * 0.5 );
        this._root.addChild( bonfire );

        let fireContainer = new PIXI.Container();
        fireContainer.scale.set( 3 );
        fireContainer.position.copyFrom( bonfire.position );
        this._root.addChild( fireContainer );

        this._fireEmitter = this.createFireEmitter( fireContainer );
    }

        private createFireEmitter( fireContainer: PIXI.Container ): particles.Emitter
        {
            let emitter: particles.Emitter = new particles.Emitter(
                fireContainer,
                [ 
                    PIXI.Loader.shared.resources[ "game_assets" ].textures[ "fire_particle_0.png" ],
                    PIXI.Loader.shared.resources[ "game_assets" ].textures[ "fire_particle_1.png" ] 
                ],
                PIXI.Loader.shared.resources[ "fire_emitter" ].data );
            emitter.autoUpdate = true;

            return emitter;
        }

    public onLeave(): void
    {        
        this._fireEmitter.destroy();
        this._fireEmitter = null;
    
        super.onLeave();
    }

    // #endregion //
}