import TaskState from "./TaskState";

import * as PIXI from "pixi.js";
import ServiceLocator from "../../ServiceLocator";


export default class TextState extends TaskState
{
    // #region Attributes //

    // private:

    private _message: PIXI.Container = null;
    private _messageGeneratorParams: MessageGeneratorParams = null;

    private _messageTimeout: number = null;

    // #endregion //


    // #region Methods //

    // public:

    public constructor() 
    { 
        super(); 
    }

    public onEnter(): void
    {
        super.onEnter();

        this._messageGeneratorParams = {
            images: [ "happy", "unicorn", "coin", "elsa" ],
            minfontSize: 40, 
            fontSizeVariation: 60,
            texts: [ "awesome", "amazing", "you did it", "great job" ] };

        this._message = new PIXI.Container();
        this._message.position.set( ServiceLocator.main.app.screen.width * 0.5,  ServiceLocator.main.app.screen.height * 0.5 );
        this._root.addChild( this._message );

        this.onMessage_Triggered();
    }

        private onMessage_Triggered(): void
        {
            this._message.removeChildren();

            // Randomly generate a combination of texts and icons.
            let arrMessageElement: Array<MessageElement> = new Array<MessageElement>();
            let textCount: number = 0;
            for ( let i: number = 0; i < 3; ++i )
            {
                if ( textCount < 2 && Math.random() >= 0.5
                    || i == 2 && textCount == 0 )
                {
                    const kText: string = this._messageGeneratorParams.texts[ Math.round( ( this._messageGeneratorParams.texts.length - 1 ) * Math.random() ) ];
                    if ( i > 0 && arrMessageElement[ i - 1 ].type == message_element_type.TEXT )
                    {
                        arrMessageElement[ i - 1 ].value += " " + kText;
                    }
                    else
                    {
                        arrMessageElement.push( { type: message_element_type.TEXT, value: kText } );
                    }
                    textCount += 1;
                }
                else
                {
                    const kIconId: string = this._messageGeneratorParams.images[ Math.round( ( this._messageGeneratorParams.images.length - 1 ) * Math.random() ) ];
                    arrMessageElement.push( { type: message_element_type.ICON, value: kIconId } );
                }
            }

            // Create the message.
            const kFontSize: number = this._messageGeneratorParams.minfontSize + Math.round( this._messageGeneratorParams.fontSizeVariation * Math.random() );
            let nextX: number = 0;
            for ( let messageElement of arrMessageElement )
            {
                if ( messageElement.type == message_element_type.TEXT )
                {
                    let text: PIXI.Text = new PIXI.Text( messageElement.value, { fontSize: kFontSize, fill: "white" } );
                    text.anchor.y = 0.5;
                    text.x = nextX;
                    this._message.addChild( text )

                    nextX += text.width + kFontSize * 0.5;
                }
                else if ( messageElement.type == message_element_type.ICON )
                {
                    let icon: PIXI.Sprite = PIXI.Sprite.from( PIXI.Loader.shared.resources[ "game_assets" ].textures[ messageElement.value + ".png" ] );
                    icon.anchor.y = 0.5;
                    const kScale = kFontSize / Math.max( icon.texture.width, icon.texture.height );
                    icon.scale.set( kScale );
                    icon.x = nextX;
                    this._message.addChild( icon )

                    nextX += icon.width + kFontSize * 0.5;
                }
            }
            this._message.x = ( ServiceLocator.main.app.screen.width - this._message.width ) * 0.5;

            this._messageTimeout = window.setTimeout( this.onMessage_Triggered.bind( this ), 2000 );
        }

    public onLeave(): void
    {        
        this._messageGeneratorParams = null;

        if ( this._messageTimeout )
        {
            clearTimeout( this._messageTimeout );
            this._messageTimeout = null;
        }

        this._message = null;
    
        super.onLeave();
    }

    // #endregion //
}

interface MessageGeneratorParams
{
    images: Array<string>;
    minfontSize: number;
    fontSizeVariation: number;
    texts: Array<string>;
}

const enum message_element_type
{
    TEXT = 0,
    ICON
}

interface MessageElement
{
    value: string;
    type: message_element_type;
}