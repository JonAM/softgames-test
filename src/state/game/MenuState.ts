import State from "../../lib/State";

import * as signals from "signals";
import * as PIXI from "pixi.js";
import ServiceLocator from "../../ServiceLocator";
import { game_state_id } from "../GameState";


export default class MenuState extends State
{
    // #region Attributes //

    // private:

    private _root: PIXI.Container = null;
    
    // Signals.
    private _onComplete: signals.Signal = new signals.Signal();

    // #endregion //


    // #region Properties //

    // Signals.
    public get onComplete(): signals.Signal { return this._onComplete; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor() 
    { 
        super(); 
    }

    public end(): void
    {
        this._onComplete.removeAll();

        super.end();
    }

    public onEnter(): void
    {
        this._root = new PIXI.Container();
        ServiceLocator.main.app.stage.addChild( this._root );

        let fullscreenButton: PIXI.Sprite = PIXI.Sprite.from( PIXI.Loader.shared.resources[ "game_assets" ].textures[ "fullscreen.png" ] );
        fullscreenButton.interactive = true;
        fullscreenButton.buttonMode = true;
        fullscreenButton.anchor.set( 1, 0 );
        fullscreenButton.scale.set( 0.5 );
        fullscreenButton.on( "pointerup", this.onFullscreenButton_Click, this );
        fullscreenButton.position.set( ServiceLocator.main.app.screen.width - 50, 50 );
        this._root.addChild( fullscreenButton );

        const kArrTitle: Array<string> = [ "Cards", "Text", "Fire" ];
        const kArrGameStateId: Array<game_state_id> = [ game_state_id.CARDS, game_state_id.TEXT, game_state_id.PARTICLES ];
        let buttonY: number = -150;
        for ( let i: number = 0; i < 3; ++i )
        {
            let button: PIXI.Sprite = this.createButton( kArrTitle[ i ], kArrGameStateId[ i ] );
            button.position.x = ServiceLocator.main.app.screen.width * 0.5;
            button.position.y = ServiceLocator.main.app.screen.height * 0.5 + buttonY + 150 * i;
            this._root.addChild( button );
        }
    }

        private onFullscreenButton_Click(): void
        {
            if ( !document.fullscreenElement ) 
            {
                document.documentElement.requestFullscreen();
            } 
            else 
            {
                if ( document.exitFullscreen ) 
                {
                    document.exitFullscreen();
                }
            }
        }

        private createButton( title: string, gameStateId: game_state_id ): PIXI.Sprite
        {
            let button: PIXI.Sprite = PIXI.Sprite.from( PIXI.Loader.shared.resources[ "game_assets" ].textures[ "button.png" ] );
            button.interactive = true;
            button.buttonMode = true;
            button.anchor.set( 0.5 );
            button.on( "pointerup", this.onTask_Selected.bind( this, gameStateId ) );
            
            let text: PIXI.Text = new PIXI.Text( title, { fontSize: 42, fill: "white" } );
            text.anchor.set( 0.5 );
            button.addChild( text );
           
            return button;
        }

    public onLeave(): void
    {
        ServiceLocator.main.app.stage.removeChild( this._root );
        this._root.destroy( { children: true } );
        this._root = null;
    
        super.onLeave();
    }

    // #endregion //

    private onTask_Selected( gameStateId: game_state_id ): void
    {
        this._onComplete.dispatch( gameStateId );
    }
}