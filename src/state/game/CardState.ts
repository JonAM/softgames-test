import TaskState from "./TaskState";

import * as PIXI from "pixi.js";
import gsap, { Sine } from "gsap";
import ServiceLocator from "../../ServiceLocator";


export default class CardState extends TaskState
{
    // #region Attributes //

    // private:

    private _topDeckContainer: PIXI.ParticleContainer = null;
    private _bottomDeckContainer: PIXI.ParticleContainer = null; // use another container to avoid issues with z-index while moving cards.

    private _movedCardCount: number = null;
    private _moveCardTimeout: number = null;

    private _arrTweeningCard: Array<PIXI.Sprite> = null; // for cleaning up when leaving this state.

    private _fpsText: PIXI.Text = null;

    // #endregion //


    // #region Methods //

    // public:

    public constructor() 
    { 
        super(); 
    }

    public onEnter(): void
    {
        super.onEnter();

        this._movedCardCount = 0;
        this._arrTweeningCard = new Array<PIXI.Sprite>();

        // Particle containers for superb performance!
        this._topDeckContainer = new PIXI.ParticleContainer( 144 );
        this._root.addChild( this._topDeckContainer );
        this._bottomDeckContainer = new PIXI.ParticleContainer( 144 );
        this._root.addChild( this._bottomDeckContainer );

        // Create the fps indicator.
        this._fpsText = new PIXI.Text( "", { fill: "white", fontSize: 60 } );
        this._root.addChild( this._fpsText );

        // Setup the top deck.
        const kTopCardX: number = ServiceLocator.main.app.screen.width * 0.5 - ( 143 * 5 ) * 0.5;
        for ( let i: number = 0; i < 144; ++i )
        {
            let card: PIXI.Sprite = PIXI.Sprite.from( PIXI.Loader.shared.resources[ "game_assets" ].textures[ "poker_card.png" ] );
            card.anchor.set( 0.5 );
            card.position.set( kTopCardX + i * 5, ServiceLocator.main.app.screen.height * 0.5 - card.height );
            this._topDeckContainer.addChildAt( card, 0 );
        }

        this.moveTopCard();
    }

        private moveTopCard(): void
        {
            const kMoveToX: number = ServiceLocator.main.app.screen.width * 0.5 + ( 143 * 5 ) * 0.5 - this._movedCardCount * 5;
            
            this._movedCardCount += 1;

            let card: PIXI.Sprite = this._topDeckContainer.children.pop() as PIXI.Sprite;
            this._bottomDeckContainer.addChild( card );
            gsap.to( card, { 
                x: kMoveToX, 
                y: ServiceLocator.main.app.screen.height * 0.5 + card.height, 
                duration: 2, 
                ease: Sine.easeOut,
                onComplete: () => { this.removeTweeningCard( card ); },
                callbackScope: this } );
            this._arrTweeningCard.push( card );

            // Move another card.
            if ( this._movedCardCount < 144 )
            {
                this._moveCardTimeout = window.setTimeout( this.moveTopCard.bind( this ), 1000 );
            }
        }

            private removeTweeningCard( card: PIXI.Sprite ): void
            {
                const kIndex: number = this._arrTweeningCard.indexOf( card );
                if ( kIndex >= 0 )
                {
                    this._arrTweeningCard.splice( kIndex, 1 );
                }
            }

    public onLeave(): void
    {        
        // Cleanup pending animations.
        for ( let card of this._arrTweeningCard )
        {
            gsap.killTweensOf( card );
        }
        this._arrTweeningCard = null;

        if ( this._moveCardTimeout )
        {
            clearTimeout( this._moveCardTimeout );
            this._moveCardTimeout = null;
        }
    
        super.onLeave();
    }

    public onUpdate( dt: number ): void
    {
        this._fpsText.text = "FPS: " + PIXI.Ticker.shared.FPS.toFixed( 2 );
    }

    // #endregion //
}