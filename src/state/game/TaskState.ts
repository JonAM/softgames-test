import State from "../../lib/State";

import * as PIXI from "pixi.js";
import * as signals from "signals";
import ServiceLocator from "../../ServiceLocator";


export default abstract class TaskState extends State
{
    // #region Attributes //

    // protected:

    protected _root: PIXI.Container = null;
    
    // Signals.
    protected _onComplete: signals.Signal = new signals.Signal();

    // #endregion //


    // #region Properties //

    // Signals.
    public get onComplete(): signals.Signal { return this._onComplete; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor() 
    { 
        super(); 
    }

    public end(): void
    {
        this._onComplete.removeAll();

        super.end();
    }

    public onEnter(): void
    {
        this._root = new PIXI.Container();
        ServiceLocator.main.app.stage.addChild( this._root );

        let backButton: PIXI.Sprite = PIXI.Sprite.from( PIXI.Loader.shared.resources[ "game_assets" ].textures[ "back.png" ] );
        backButton.interactive = true;
        backButton.buttonMode = true;
        backButton.anchor.set( 1, 0 );
        backButton.scale.set( 0.5 );
        backButton.on( "pointerup", () => { this._onComplete.dispatch() } );
        backButton.position.set( ServiceLocator.main.app.screen.width - 50, 50 );
        this._root.addChild( backButton );
    }

    public onLeave(): void
    {        
        ServiceLocator.main.app.stage.removeChild( this._root );
        this._root.destroy( { children: true } );
        this._root = null;
    
        super.onLeave();
    }

    // #endregion //
}