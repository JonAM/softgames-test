import State from "../lib/State";

import StateMachine from "../lib/StateMachine";
import CardState from "./game/CardState";
import MenuState from "./game/MenuState";
import ParticleState from "./game/ParticleState";
import TextState from "./game/TextState";


export default class GameState extends State
{
    // #region Attributes //

    // private:

    private _stateMachine: StateMachine = null; 

    // #endregion //


    // #region Methods //

    // public:

    public constructor() 
    { 
        super(); 
    }

    public onEnter(): void
    {
        this._stateMachine = new StateMachine();
        //
        let menuState: MenuState = new MenuState();
        menuState.onComplete.add( this.onTask_Selected, this );
        this._stateMachine.add( game_state_id.MENU, menuState );
        //
        let cardState: CardState = new CardState();
        cardState.onComplete.add( this.onBack_Selected, this );
        this._stateMachine.add( game_state_id.CARDS, cardState );
        //
        let textState: TextState = new TextState();
        textState.onComplete.add( this.onBack_Selected, this );
        this._stateMachine.add( game_state_id.TEXT, textState );
        //
        let particleState: ParticleState = new ParticleState();
        particleState.onComplete.add( this.onBack_Selected, this );
        this._stateMachine.add( game_state_id.PARTICLES, particleState );
        //
        this._stateMachine.init();
        
        this._stateMachine.request( game_state_id.MENU );
    }

        private onTask_Selected( gameStateId: game_state_id ): void
        {
            this._stateMachine.request( gameStateId );
        }

        private onBack_Selected(): void
        {
            this._stateMachine.request( game_state_id.MENU );
        }

    public onLeave(): void
    {
        this._stateMachine.end();
        this._stateMachine = null;
    
        super.onLeave();
    }

    public onUpdate( dt: number ): void
    {
        this._stateMachine.onUpdate( dt );
    }

    // #endregion //
}

export const enum game_state_id
{
    MENU = 0,
    CARDS,
    TEXT,
    PARTICLES
}