export default abstract class State
{
    // #region Methods //

    // public:

    // virtual.
    public init(): void {};
    public end(): void {};

    // virtual.
    public onEnter(): void {}
    public onLeave(): void {}
    public onUpdate( dt: number ): void {}

    // #endregion //
}