import State from "./State";


export default class StateMachine
{
    // #region Attributes //

    // private:

    private _mapState: Map<number, State> = new Map<number, State>();
    private _currentState: State = null;

    // #endregion //


    // #region Methods //

    // public:

    public init(): void
    {
        this._mapState.forEach( ( value: State, key: number ) => { value.init(); } );
    }

    public end(): void
    {
        if ( this._currentState )
        {
            this._currentState.onLeave();

            this._currentState = null;
        }

        this._mapState.forEach( ( value: State, key: number ) => { value.end(); } );
    }

    public add( stateId: number, state: State ): void
    {
        this._mapState.set( stateId, state );
    }

    public request( stateId: number ): void
    {
        if ( this._currentState != null )
        {
            this._currentState.onLeave();
        }

        this._currentState = this._mapState.get( stateId );

        this._currentState.onEnter();
    }

    public onUpdate( dt: number ): void
    {
        if ( this._currentState != null )
        {
            this._currentState.onUpdate( dt );
        }
    }

    // #endregion //
}