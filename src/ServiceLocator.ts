import Main from "./Main";


export default class ServiceLocator
{
    // #region Attributes //

    // private:

    private static _gMain: Main = null;

    // TODO: Add more global services here.
    
    // #endregion //


    // #region Properties //

    public static set main( value: Main ) { this._gMain = value; }

    public static get main(): Main { return this._gMain; }

    // #endregion


    // #region Methods //

    // private:

    private constructor() {}

    // #endregion //
}